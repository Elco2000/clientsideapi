const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const routes = require('./src/routes/routes');
const cors = require('cors');
const app = express();

// mongoose.Promise = global.Promise;
// if (process.env.NODE_ENV !== 'test') {
//     mongoose.connect('mongodb+srv://elco:Elcomussert85!@apidata.twggy.mongodb.net/efootball?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
// }


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors())
routes(app);

module.exports = app;
