const assert = require('assert');
const request = require('supertest');
const app = require('../../app');
const mongoose = require('mongoose');
const Player = require('../../src/models/player.schema')
const jwt = require("jsonwebtoken");
const Team = mongoose.model('team');

describe('Players controller', () => {
    it('PUT to /api/matches/:id/players/create create a player', (done) => {
        const team = new Team({
            name: 'VVV-Venlo',
            foundedOn: new Date(2000, 1, 1),
            stadium: 'De Venlo',
            nickname: 'Club van Venlo',
            professionalStatus: 'Medium Professional',
            coach: 'Elco Mussert',
            players: []
        });
        const player = ({
            firstname: 'Elco',
            lastname: 'Mussert',
            nationality: 'Netherlands',
            dateOfBirth: new Date(2000, 2, 1),
            captain: true,
            positions: [
                'Forward Centre'
            ]
        });

        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            team.save().then(() => {
                request(app)
                    .put(`/api/teams/${team._id}/players/create`)
                    .set("authorization", "Bearer " + token)
                    .send(player)
                    .end(() => {
                        Team.findOne({name: 'VVV-Venlo'})
                            .then(team => {
                                assert(team.players[0].firstname === 'Elco');
                                done();
                            });
                    });
            });
        });
    });

    it('PUT to /api/matches/:id/players/:playerid edit a player', (done) => {
        const player = ({
            positions: [
                "Defender Centre"
            ],
            _id: "5fcd07f404929b8358bab056",
            firstname: "Freek",
            lastname: "Mussert",
            nationality: "Netherlands",
            dateOfBirth: "2000-05-25T22:00:00.000Z",
            captain: true
        });
        const team = new Team({
            name: 'VVV-Venlo',
            foundedOn: new Date(2000, 1, 1),
            stadium: 'De Venlo',
            nickname: 'Club van Venlo',
            professionalStatus: 'Medium Professional',
            coach: 'Elco Mussert',
            players: [player]
        });

        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            team.save().then(() => {
                request(app)
                    .put(`/api/teams/${team._id}/players/${player._id}`)
                    .set("authorization", "Bearer " + token)
                    .send({firstname: "Willem"})
                    .end(() => {
                        Team.findOne({_id: team._id})
                            .then(team => {
                                assert(team.players[0].firstname === 'Willem');
                                done();
                            })
                    });
            });
        });
    });

    it('DELETE to /api/matches/:id/players/:playerid delete a player', (done) => {
        const player = ({
            positions: [
                "Defender Centre"
            ],
            _id: "5fcd07f404929b8358bab056",
            firstname: "Freek",
            lastname: "Mussert",
            nationality: "Netherlands",
            dateOfBirth: "2000-05-25T22:00:00.000Z",
            captain: true
        });
        const team = new Team({
            name: 'VVV-Venlo',
            foundedOn: new Date(2000, 1, 1),
            stadium: 'De Venlo',
            nickname: 'Club van Venlo',
            professionalStatus: 'Medium Professional',
            coach: 'Elco Mussert',
            players: [player]
        });

        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            team.save().then(() => {
                request(app)
                    .delete(`/api/teams/${team._id}/players/${player._id}`)
                    .set("authorization", "Bearer " + token)
                    .end(() => {
                        Team.findOne({_id: team._id})
                            .then(team => {
                                assert(team.players[0] === undefined);
                                done();
                            })
                    });
            });
        });
    });

    it('GET to /api/teams/:id/players can get all players', (done) => {
        const player = ({
            positions: [
                "Defender Centre"
            ],
            _id: "5fcd07f404929b8358bab056",
            firstname: "Freek",
            lastname: "Mussert",
            nationality: "Netherlands",
            dateOfBirth: "2000-05-25T22:00:00.000Z",
            captain: true
        });
        const team = new Team({
            name: 'VVV-Venlo',
            foundedOn: new Date(2000, 1, 1),
            stadium: 'De Venlo',
            nickname: 'Club van Venlo',
            professionalStatus: 'Medium Professional',
            coach: 'Elco Mussert',
            players: [player]
        });

        team.save()
            .then(() => {
                request(app)
                    .get(`/api/teams/${team._id}/players`)
                    .end(() => {
                        Team.findOne({_id: team._id})
                            .then((team) => {
                                assert(team.players.length === 1);
                                done();
                            });
                    });
            });
    });

    it('GET to /api/teams/:id/players/:playerid can get one player', (done) => {
        const player = ({
            positions: [
                "Defender Centre"
            ],
            _id: "5fcd07f404929b8358bab056",
            firstname: "Freek",
            lastname: "Mussert",
            nationality: "Netherlands",
            dateOfBirth: "2000-05-25T22:00:00.000Z",
            captain: true
        });
        const team = new Team({
            name: 'VVV-Venlo',
            foundedOn: new Date(2000, 1, 1),
            stadium: 'De Venlo',
            nickname: 'Club van Venlo',
            professionalStatus: 'Medium Professional',
            coach: 'Elco Mussert',
            players: [player]
        });

        team.save()
            .then(() => {
                request(app)
                    .get(`/api/teams/${team._id}/players/${player._id}`)
                    .end(() => {
                        Team.findOne({_id: team._id})
                            .then((team) => {
                                assert(team.players.length === 1);
                                assert(team.players[0].firstname === 'Freek');
                                done();
                            });
                    });
            });
    });
});
