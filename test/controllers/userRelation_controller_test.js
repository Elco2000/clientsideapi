const assert = require('assert');
const request = require('supertest');
const app = require('../../app');
const session = require("../../neo");
const jwt = require("jsonwebtoken");

xdescribe ('User Relation Controller', () => {
    beforeEach((done) => {
        const body = {name: 'test1', password: '123456', email: 'test1@mail.nl'}
        session.session()
            .run('CREATE (a:User { name: $name, password: $password, email: $email}) RETURN a',
                body
            )
            .then(() => session.session().close())

        const bodyTwo = {name: 'test2', password: '123456', email: 'test2@mail.nl'}
        session.session()
            .run('CREATE (a:User { name: $name, password: $password, email: $email}) RETURN a',
                bodyTwo
            )
            .then(() => {
                session.session().close();
            })
        done();

    })


    it('Post to /api/relation creates a new friendship', (done) => {
        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            request(app)
                .post('api/relation')
                .set("authorization", "Bearer " + token)
                .send({
                    aName: 'test1',
                    bName: 'test2'
                })
                .end((err, res) => {
                    if (err) { console.error(err) }
                    res.should.have.status(200)
                    // res.body.should.be.a('array')
                    done();
                })
        })
    })
})
