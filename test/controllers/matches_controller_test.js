const assert = require('assert');
const request = require('supertest');
const app = require('../../app');
const mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const Team = mongoose.model('team');
const Match = mongoose.model('match');

describe('Matches controller', () => {
    let homeTeamMatch, awayTeamMatch

    beforeEach((done) => {
        homeTeamMatch = new Team({
            name: 'Feyenoord',
            foundedOn: new Date(2000, 1, 1),
            stadium: 'De Kuip',
            nickname: 'Club van Zuid',
            professionalStatus: 'High Professional',
            coach: 'Dick Advocaat'
        });
        awayTeamMatch = new Team({
            name: 'FeyenoordTwo',
            foundedOn: new Date(2000, 1, 1),
            stadium: 'De Kuip',
            nickname: 'Club van Zuid',
            professionalStatus: 'High Professional',
            coach: 'Duck Advocaat'
        });

        Promise.all([homeTeamMatch.save(), awayTeamMatch.save()])
            .then(() => done());
    });

    it('Post to /api/matches creates a new match', (done) => {
        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            Match.countDocuments().then(count => {
                request(app)
                    .post('/api/matches')
                    .set("authorization", "Bearer " + token)
                    .send({
                        homeTeam: homeTeamMatch._id,
                        awayTeam: awayTeamMatch._id,
                        result: '1-1',
                        matchDate: new Date(2020, 12, 2)
                    })
                    .end(() => {
                        Match.countDocuments().then(newCount => {
                            assert(count + 1 === newCount);
                            done();
                        });
                    });
            });
        })
    });

    it('PUT to /api/teams/id edits an existing team', (done) => {
        const match = new Match({
            homeTeam: homeTeamMatch._id,
            awayTeam: awayTeamMatch._id,
            result: '1-1',
            matchDate: new Date(2020, 12, 2)
        });

        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            match.save().then(() => {
                request(app)
                    .put(`/api/matches/${match._id}`)
                    .set("authorization", "Bearer " + token)
                    .send({result: '2-1'})
                    .end(() => {
                        Match.findOne({result: '2-1'})
                            .then(match => {
                                assert(match.result === '2-1');
                                done();
                            });
                    });
            });
        })
    });

    it('DELETE to /api/matches/id can delete a match', (done) => {
        const match = new Match({
            homeTeam: homeTeamMatch._id,
            awayTeam: awayTeamMatch._id,
            result: '1-1',
            matchDate: new Date(2020, 12, 2)
        });

        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            match.save().then(() => {
                request(app)
                    .delete(`/api/matches/${match._id}`)
                    .set("authorization", "Bearer " + token)
                    .end(() => {
                        Match.findOne({result: '1-1'})
                            .then((match) => {
                                assert(match === null);
                                done();
                            });
                    });
            });
        })
    });

    it('GET to /api/matches can get all match', (done) => {
        const match = new Match({
            homeTeam: homeTeamMatch._id,
            awayTeam: awayTeamMatch._id,
            result: '1-1',
            matchDate: new Date(2020, 12, 2)
        });

        match.save()
            .then(() => {
                request(app)
                    .get('/api/matches')
                    .end(() => {
                        Match.find()
                            .then((matches) => {
                                assert(matches.length === 1);
                                assert(matches[0].result === '1-1');
                                done();
                            });
                    });
            });
    });

    it('GET to /api/matches/id can get one specific match', (done) => {
        const match = new Match({
            homeTeam: homeTeamMatch._id,
            awayTeam: awayTeamMatch._id,
            result: '1-1',
            matchDate: new Date(2020, 12, 2)
        });

        match.save()
            .then(() => {
                request(app)
                    .get(`/api/matches/${match._id}`)
                    .end(() => {
                        Match.findOne({_id: match._id})
                            .then((match) => {
                                assert(match.result === '1-1');
                                done();
                            });
                    });
            });
    });
});
