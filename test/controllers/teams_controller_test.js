const assert = require('assert');
const request = require('supertest');
const app = require('../../app');
const mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const Team = mongoose.model('team');

describe('Teams controller', () => {
    it('Post to /api/teams creates a new player', (done) => {
        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            Team.countDocuments().then(count => {
                request(app)
                    .post('/api/teams')
                    .set("authorization", "Bearer " + token)
                    .send({
                        name: 'Feyenoord',
                        foundedOn: new Date(2000, 1, 1),
                        stadium: 'De Kuip',
                        nickname: 'Club van Zuid',
                        professionalStatus: 'High Professional',
                        coach: 'Dick Advocaat'
                    })
                    .end(() => {
                        Team.countDocuments().then(newCount => {
                            assert(count + 1 === newCount);
                            done();
                        });
                    });
            });
        });
    });

    it('PUT to /api/teams/id edits an existing team', (done) => {
        const team = new Team({
            name: 'VVV-Venlo',
            foundedOn: new Date(2000, 1, 1),
            stadium: 'De Venlo',
            nickname: 'Club van Venlo',
            professionalStatus: 'Medium Professional',
            coach: 'Elco Mussert'
        });

        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            team.save().then(() => {
                request(app)
                    .put(`/api/teams/${team._id}`)
                    .set("authorization", "Bearer " + token)
                    .send({coach: 'Joris Wessels'})
                    .end(() => {
                        Team.findOne({name: 'VVV-Venlo'})
                            .then(team => {
                                assert(team.coach === 'Joris Wessels');
                                done();
                            });
                    });
            });
        });
    });

    it('DELETE to /api/teams/id can delete a team',  (done) => {
        const team = new Team({
            name: 'VVV-Venlo',
            foundedOn: new Date(2000, 1, 1),
            stadium: 'De Venlo',
            nickname: 'Club van Venlo',
            professionalStatus: 'Medium Professional',
            coach: 'Elco Mussert'
        });

        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            team.save().then(() => {
                request(app)
                    .delete(`/api/teams/${team._id}`)
                    .set("authorization", "Bearer " + token)
                    .end(() => {
                        Team.findOne({name: 'VVV-Venlo'})
                            .then((team) => {
                                assert(team === null);
                                done();
                            });
                    });
            });
        });
    });

    it('GET to /api/teams can get all teams', (done) => {
        const team = new Team({
            name: 'VVV-Venlo',
            foundedOn: new Date(2000, 1, 1),
            stadium: 'De Venlo',
            nickname: 'Club van Venlo',
            professionalStatus: 'Medium Professional',
            coach: 'Elco Mussert'
        });

        team.save()
            .then(() => {
                request(app)
                    .get('/api/teams')
                    .end(() => {
                        Team.find()
                            .then((teams) => {
                                assert(teams.length === 1);
                                assert(teams[0].name === 'VVV-Venlo');
                                done();
                            });
                    });
            });
    });

    it('GET to /api/teams/id can get one specific team', (done) => {
        const team = new Team({
            name: 'VVV-Venlo',
            foundedOn: new Date(2000, 1, 1),
            stadium: 'De Venlo',
            nickname: 'Club van Venlo',
            professionalStatus: 'Medium Professional',
            coach: 'Elco Mussert'
        });

        team.save()
            .then(() => {
                request(app)
                    .get(`/api/teams/${team._id}`)
                    .end(() => {
                        Team.findOne({_id: team._id})
                            .then((team) => {
                                assert(team.name === 'VVV-Venlo');
                                done();
                            });
                    });
            });
    });
});
