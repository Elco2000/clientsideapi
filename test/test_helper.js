const mongoose = require('mongoose');
const connect = require('../connect');
require('dotenv').config()

before(done => {
    mongoose.connect('mongodb+srv://elco:Elcomussert85!@apidata.twggy.mongodb.net/efootball_test?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    });
    mongoose.connection
        .once('open', () => done())
        .on('error', err => {
            console.warn('Warning', err)
        });
    connect.neo(process.env.NEO4J_TEST_DB)
});

beforeEach(done => {
    const {teams} = mongoose.connection.collections;
    const {matches} = mongoose.connection.collections;

    Promise.all([teams.drop(), matches.drop()])
        .then(() => done())
        .catch(() => done());
});
