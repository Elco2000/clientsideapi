const PlayersController = require('../controllers/players_controller')
const TeamsController = require('../controllers/teams_controller')
const MatchesController = require('../controllers/matches_controller')
const AuthController = require('../controllers/auth_controller')
const userRelationController = require('../controllers/userRelation_controller')
const userController = require('../controllers/user_controller')

module.exports = (app) => {
    // app.get('/api', PlayersController.test);

    // app.post('/api/players', PlayersController.create);

    app.use(function (req, res, next) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader(
            "Access-Control-Allow-Methods",
            "GET, POST, OPTIONS, PUT, PATCH, DELETE"
        );
        res.setHeader(
            "Access-Control-Allow-Headers",
            "X-Requested-With,content-type,authorization"
        );
        res.setHeader("Access-Control-Allow-Credentials", true);
        next();
    });

    // Teams
    app.post('/api/teams', AuthController.validateToken, TeamsController.create);
    app.put('/api/teams/:id', AuthController.validateToken, TeamsController.edit);
    app.delete('/api/teams/:id', AuthController.validateToken, TeamsController.delete);
    app.get('/api/teams', TeamsController.get);
    app.get('/api/teams/:id', TeamsController.getById);

    // Matches
    app.post('/api/matches', AuthController.validateToken, MatchesController.create);
    app.put('/api/matches/:id', AuthController.validateToken, MatchesController.edit);
    app.delete('/api/matches/:id', AuthController.validateToken, MatchesController.delete);
    app.get('/api/matches', MatchesController.get);
    app.get('/api/matches/recent', MatchesController.getWithScore);
    app.get('/api/matches/upcoming', MatchesController.getWithoutScore);
    app.get('/api/matches/:id', MatchesController.getById);

    // Players
    app.put('/api/teams/:id/players/create', AuthController.validateToken, PlayersController.create);
    app.put('/api/teams/:id/players/:playerid', AuthController.validateToken, PlayersController.edit);
    app.get('/api/teams/:id/players/', PlayersController.get);
    app.get('/api/teams/:id/players/:playerid', PlayersController.getById);
    app.delete('/api/teams/:id/players/:playerid', AuthController.validateToken, PlayersController.delete);

    // Auth
    app.post('/api/register', AuthController.register);
    app.post('/api/login', AuthController.login);

    //User Relation and follow functions
    app.put('/api/relation/:aId/:id', AuthController.validateToken, userRelationController.makeRelation);
    app.get('/api/relation/if/:id', AuthController.validateToken, userRelationController.showRelationOfPerson);
    app.get('/api/relation/af/:id', AuthController.validateToken, userRelationController.showRelationOfAnother);
    app.put('/api/relation/delete/:aId/:id', AuthController.validateToken, userRelationController.deleteRelation);
    app.get('/api/relation/checkstatus/:aId/:id', AuthController.validateToken, userRelationController.checkForFollowStatus);

    //User
    app.get('/api/users', userController.getAllUsers);
    app.get('/api/users/:id', userController.getUserById)
};
