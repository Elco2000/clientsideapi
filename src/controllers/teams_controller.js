const Team = require('../models/team');

module.exports = {
    get(req, res, next) {
        Team.find({})
            .then(teams => res.send(teams))
            .catch(next);
    },

    getById(req, res, next) {
        const teamId = req.params.id;
        Team.findById({_id: teamId}, {name: 1, nickname: 1, foundedOn: 1, stadium: 1, professionalStatus: 1, coach: 1})
            .then(team => res.send(team))
            .catch(next);
    },

    create(req, res, next) {
        const teamProps = req.body;

        Team.create(teamProps)
            .then(team => res.send(team))
            .catch(next);
    },

    edit(req, res, next) {
        const teamId = req.params.id;
        const teamProps = req.body;

        Team.findByIdAndUpdate({_id: teamId}, teamProps)
            .then(() => Team.findById({_id: teamId}))
            .then(team => res.send(team))
            .catch(next);
    },

    delete(req, res, next) {
        const teamId = req.params.id;

        Team.findByIdAndRemove({_id: teamId})
            .then(team => res.status(204).send(team))
            .catch(next);
    }
};
