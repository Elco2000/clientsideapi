const Match = require('../models/match');

module.exports = {
    get(req, res, next) {
        Match.find({})
            .populate("homeTeam", {name: 1, stadium: 1})
            .populate("awayTeam", {name: 1, stadium: 1})
            .then(matches => res.send(matches))
            .catch(next);
    },

    getById(req, res, next) {
        const matchId = req.params.id;
        Match.findById({_id: matchId})
            .populate("homeTeam", {name: 1, stadium: 1})
            .populate("awayTeam", {name: 1, stadium: 1})
            .then(match => res.send(match))
            .catch(next);
    },

    create(req, res, next) {
        const matchProps = req.body;

        Match.create(matchProps)
            .then(match => res.send(match))
            .catch(next);
    },

    edit(req, res, next) {
        const matchId = req.params.id;
        const matchProps = req.body;

        Match.findByIdAndUpdate({_id: matchId}, matchProps)
            .then(() => Match.findById({_id: matchId}))
            .then(match => res.send(match))
            .catch(next);
    },

    delete(req, res, next) {
        const matchId = req.params.id;

        Match.findByIdAndRemove({_id: matchId})
            .then(match => res.status(204).send(match))
            .catch(next);
    },

    getWithScore(req, res, next) {
        Match.aggregate([
            {
                $lookup: {
                    from: 'teams',
                    localField: 'homeTeam',
                    foreignField: '_id',
                    as: 'homeTeam'
                }
            },
            {$unwind: "$homeTeam"},
            {
                $lookup: {
                    from: 'teams',
                    localField: 'awayTeam',
                    foreignField: '_id',
                    as: 'awayTeam'
                }
            },
            {$unwind: "$awayTeam"},
            {
              $project: {
                  homeTeam: {
                      _id: 1,
                      name: 1,
                      stadium: 1
                  },
                  awayTeam: {
                      _id: 1,
                      name: 1,
                      stadium: 1,
                  },
                  result: 1,
                  matchDate: 1
              }
            },
            {
                $match: {
                    result: {$ne: ""}
                }
            },
            {
                $sort: {
                    matchDate: 1
                }
            }
        ])
            .then(matches => res.send(matches))
            .catch(next);
    },

    getWithoutScore(req, res, next) {
        Match.aggregate([
            {
                $lookup: {
                    from: 'teams',
                    localField: 'homeTeam',
                    foreignField: '_id',
                    as: 'homeTeam'
                }
            },
            {$unwind: "$homeTeam"},
            {
                $lookup: {
                    from: 'teams',
                    localField: 'awayTeam',
                    foreignField: '_id',
                    as: 'awayTeam'
                }
            },
            {$unwind: "$awayTeam"},
            {
                $project: {
                    homeTeam: {
                        _id: 1,
                        name: 1,
                        stadium: 1
                    },
                    awayTeam: {
                        _id: 1,
                        name: 1,
                        stadium: 1,
                    },
                    result: 1,
                    matchDate: 1
                }
            },
            {
                $match: {
                    result: ""
                }
            },
            {
                $sort: {
                    matchDate: 1
                }
            }
        ])
            .then(matches => res.send(matches))
            .catch(next);
    },
};
