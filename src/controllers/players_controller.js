const Player = require('../models/player.schema');
const Team = require('../models/team');

module.exports = {
    create(req, res, next) {
        const teamId = req.params.id;
        const playerProps = req.body;

        Team.findByIdAndUpdate({_id: teamId}, {$push: {players: playerProps}})
            .then(() => Team.findById({_id: teamId}))
            .then(team => res.send(team))
            .catch(next);
    },

    edit(req, res, next) {
        const teamId = req.params.id;
        const playerId = req.params.playerid;
        const playerProps = req.body;

        Team.findById({"_id": teamId, "players._id": playerId })
            .then(() => Team.findById({_id: teamId}))
            .then(team => {
                const player = team.players.id(playerId);
                player.set(playerProps);
                return team.save();
            })
            .then((team) => {
                res.send(team);
            })
            .catch(next);
    },

    getById(req, res, next) {
        const teamId = req.params.id;
        const playerId = req.params.playerid;

        Team.findById({"_id": teamId, "players._id": playerId}, {players: 1})
            .then(player => res.send(player))
            .catch(next);
    },

    delete(req, res, next) {
        const teamId = req.params.id;
        const playerId = req.params.playerid;


        Team.findById({"_id": teamId, "players._id": playerId})
            .then(() => Team.findById({_id: teamId}))
            .then(team => {
                const player = team.players.id(playerId);
                player.remove();
                return team.save();
            })
            .then(team => res.status(204).send(team))
            .catch(next);
    },

    get(req, res, next) {
        const teamId = req.params.id;
        Team.findById({_id: teamId}, {players: 1})
            .then(team => res.send(team))
            .catch(next);
    },

};
