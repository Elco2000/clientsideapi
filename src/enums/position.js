const positionEnum = [
    'Goalkeeper',
    'Defender Centre',
    'Defender Right',
    'Defender Left',
    'Defensive midfielder Centre',
    'Defensive midfielder Right',
    'Defensive midfielder Left',
    'Midfielder Centre',
    'Midfielder Right',
    'Midfielder Left',
    'Attacking midfielder Centre',
    'Attacking midfielder Right',
    'Attacking midfielder Left',
    'Forward Centre',
    'Forward Right',
    'Forward Left'
]

module.exports = positionEnum;
