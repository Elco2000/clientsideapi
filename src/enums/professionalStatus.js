const professionalStatusEnum = [
    'High Professional',
    'Medium Professional',
    'Low Professional'
];

module.exports = professionalStatusEnum;

