const mongoose = require('mongoose');
// const TeamSchema = require('./team');
const Schema = mongoose.Schema;

const MatchSchema = new Schema({
    homeTeam: {
        type: mongoose.Types.ObjectId,
        ref: 'team',
        required: true,
        validate: {
            validator: (homeTeam, awayTeam) => homeTeam !== awayTeam,
            message: 'homeTeam can not be the same as awayTeam'
        }
    },
    awayTeam: {
        type: mongoose.Types.ObjectId,
        ref: 'team',
        required: true,
        validate: {
            validator: (awayTeam, homeTeam) => awayTeam !== homeTeam,
            message: 'awayTeam can not be the same as homeTeam'
        }
    },
    result: {
        type: String,
        required: false,
        validate: {
            validator: (result) => result.length >= 3 || result.length === 0,
            message: 'Result can not be shorter then 3'
        }
    },
    matchDate: {
        type: Date,
        required: true
    }
});

const Match = mongoose.model('match', MatchSchema);

module.exports = Match;
