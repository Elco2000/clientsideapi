const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const positionEnum = require('../enums/position');
const nationalityEnum = require('../enums/nationality');

const PlayerSchema = new Schema({
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    nationality: {
        type: String,
        enum: nationalityEnum,
        required: true
    },
    dateOfBirth: {
        type: Date,
        required: true
    },
    captain: {
        type: Boolean,
        required: false
    },
    positions: {
        type: [String],
        enum: positionEnum,
        required: true
    }
});

module.exports = PlayerSchema;
