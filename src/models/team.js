const mongoose = require('mongoose');
const PlayerSchema = require('./player.schema')
const Schema = mongoose.Schema;

const professionalStatusEnum = require('../enums/professionalStatus');

const TeamSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    foundedOn: {
        type: Date,
        required: true
    },
    stadium: {
        type: String,
        required: true
    },
    nickname: {
        type: String,
        required: true
    },
    professionalStatus: {
        type: String,
        enum: professionalStatusEnum,
        required: true
    },
    coach: {
        type: String,
        required: true
    },
    players: {
        type: [PlayerSchema],
        default: [],
        required: true
    }
});

const Team = mongoose.model('team', TeamSchema);

module.exports = Team;
